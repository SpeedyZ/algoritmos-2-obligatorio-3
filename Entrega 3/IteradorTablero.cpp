#ifndef ITERADORTABLERO_CPP
#define ITERADORTABLERO_CPP

#include "IteradorTablero.h"

template<class Tablero>
IteradorTablero<Tablero>::IteradorTablero(const Tablero& tab) {
	this->tablero = tab;
	Matriz<int> fichas = tablero.ObtenerTablero();
	nat i = 0;
	nat j = 0;

	for (nat r = 0; r < fichas.ObtenerLargo(); r++) {
		for (nat s = 0; s < fichas.ObtenerAncho(); s++) {
			if (fichas[r][s] == 0) {
				i = r;
				j = s;
			}
		}
	}

	nat auxiliar = 0;
	for (int h = -1; h <= 1; h += 2) {
		nat p = i + h;
		nat q = j + h;
		if (p >= 0 && p < fichas.ObtenerLargo()) {
			Tablero aux = tablero;
			Matriz<int> vecino = aux.ObtenerTablero();
			auxiliar = vecino[p][j];
			vecino[p][j] = 0;
			vecino[i][j] = auxiliar;
			InsertarLista(Tablero(vecino));
		}
		if (q >= 0 && q < fichas.ObtenerAncho()) {
			Tablero aux = tablero;
			Matriz<int> vecino = aux.ObtenerTablero();
			auxiliar = vecino[i][q];
			vecino[i][q] = 0;
			vecino[i][j] = auxiliar;
			InsertarLista(Tablero(vecino));
		}
	}
	actual = ppio;
}

template<class Tablero>
IteradorTablero<Tablero>::IteradorTablero(const Puntero<Etapa>& et) {
	Puntero<Etapa> it = et;
	while (it != NULL) {
		InsertarLista(it->Fichas());
		it = it->EtapaAnterior();
	}
}

template<class Tablero>
bool IteradorTablero<Tablero>::HayElemento() const {
	return actual != NULL;
}

template<class Tablero>
const Tablero & IteradorTablero<Tablero>::ElementoActual() const {
	return actual->dato;
}

template<class Tablero>
void IteradorTablero<Tablero>::Avanzar() {
	actual = actual->sig;
}

template<class Tablero>
void IteradorTablero<Tablero>::Reiniciar() {
	actual = ppio;
}

template<class Tablero>
Puntero<Iteracion<Tablero>> IteradorTablero<Tablero>::Clonar() const {
	return new IteradorTablero(this->tablero);
}

template<class Tablero>
void IteradorTablero<Tablero>::InsertarLista(const Tablero & tab) {
	Puntero<NodoLista<Tablero>> nuevo = new NodoLista<Tablero>(tab);
	nuevo->sig = ppio;
	ppio = nuevo;
}


#endif

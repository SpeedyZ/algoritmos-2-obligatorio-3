#ifndef TABLERO_CPP
#define TABLERO_CPP

#include "Tablero.h"
#include "IteradorTablero.h"

Tablero::Tablero (Matriz<int> bloques)
{
	copia(bloques, mat);
}

Tablero::Tablero(const Tablero & t)
{
	*this = t;
}
	
nat Tablero::PrioridadA() const
{
	int suma = 0;
	int count = 1;
	for (nat i = 0; i < mat.ObtenerLargo(); i++) {
		for (nat j = 0; j < mat.ObtenerAncho(); j++) {
			if (i == mat.ObtenerLargo() - 1 && j == mat.ObtenerAncho())
			{
				if (mat[i][j] != 0) suma++;
			}
			else if (mat[i][j] != count++) suma++;
		}
	}
	return suma;
}
	
nat Tablero::PrioridadB() const
{
	int suma = 0;
	for (nat i = 0; i < mat.ObtenerLargo(); i++) {
		for (nat j = 0; j < mat.ObtenerAncho(); j++) {
			if (mat[i][j] != 0) {
				nat posi = mat[i][j] / mat.ObtenerAncho();
				nat posj = 2;
				if (mat[i][j] % mat.ObtenerAncho() != 0) posj = mat[i][j] % mat.ObtenerAncho();
				int difi = i - posi;
				int difj = j - posj;
				suma += abs(difi) + abs(difj);
			}
		}
	}


	return suma;
}
	
bool Tablero::operator==(const Tablero& t) const
{
	bool ret = true;
	if (this != &t) {
		Matriz<int> aux = t.ObtenerTablero();
		for (nat i = 0; ret && i < aux.ObtenerLargo(); i++) {
			for (nat j = 0; ret && aux.ObtenerAncho(); j++) {
				if (mat[i][j] != aux[i][j]) ret = false;
			}
		}
	}
	return ret;
}

Tablero & Tablero::operator=(const Tablero & t)
{
	if (this != &t) copia(t.ObtenerTablero(), mat);
	return *this;
}
	
Iterador<Tablero> Tablero::Vecinos() const 
{
	return new IteradorTablero<Tablero>(mat);
}

Matriz<int> Tablero::ObtenerTablero() const
{
	return mat;
}

Cadena Tablero::Imprimir() const
{
	Cadena ret = "";
	char c[2] = { '/0' };
	for (nat i = 0; i < mat.ObtenerLargo(); i++) {
		for (nat j = 0; j < mat.ObtenerAncho(); j++) {
			c[1] = mat[i][j];
			ret + c;

		}
	}
	return ret;
}

void Tablero::copia(const Matriz<int>& original, Matriz<int>& copia) {
	nat ancho = original.ObtenerAncho();
	nat largo = original.ObtenerLargo();

	copia = Matriz<int>(largo, ancho);
	for (nat i = 0; i < largo; i++) {
		for (nat j = 0; j < ancho; j++) {
			original[i][j] = original[i][j];
		}
	}
}

#endif


#ifndef COLAPRIORIDAD_CPP
#define COLAPRIORIDAD_CPP

#include "ColaPrioridad.h"

template<class P, class T>
ColaPrioridad<P, T>::ColaPrioridad()
{
}

template<class P, class T>
ColaPrioridad<P, T>::ColaPrioridad(const ColaPrioridad<P, T>& cola)
{
}

template<class P, class T>
bool ColaPrioridad<P, T>::esVacia()
{
	return false;
}

template<class P, class T>
nat ColaPrioridad<P, T>::largo()
{
	return heap.length();
}

template<class P, class T>
T & ColaPrioridad<P, T>::Top() const
{
	// TODO: insert return statement here
}

template<class P, class T>
void ColaPrioridad<P, T>::Push(const P & prioridad, const T & dato)
{
}

template<class P, class T>
void ColaPrioridad<P, T>::Pop()
{
}

template<class P, class T>
void ColaPrioridad<P, T>::BorrarElemento(const T & dato)
{
}

template<class P, class T>
void ColaPrioridad<P, T>::CambiarPrioridad(const P & prioridad, const T & dato)
{
}

template<class P, class T>
void ColaPrioridad<P, T>::CambiarPrioridadTodos(const P& prioridad)
{
}


#endif 
#ifndef COLAPRIORIDADIMP_H
#define COLAPRIORIDADIMP_H

#define TAM_COLA 101
#include "Iterable.h"
#include "Puntero.h"
#include "ColaPrioridad.h"
#include "Tupla.h"
#include "Array.h"
#include "Comparador.h"

typedef unsigned int Natural;

template <class T, class P>
class ColaPrioridadImp : public ColaPrioridad<T, P>
{
public:
	~ColaPrioridadImp();
	ColaPrioridadImp(Comparador<T> compT, Comparador<P> compP);

	// PRE: -
	// POS: Encola el elemento e con prioridad p
	void Encolar(const T& e, const P& p);

	// PRE: La cola no est� vac�a
	// POS: Retorna el elemento de mayor prioridad en la cola elimin�ndolo
	const T& Desencolar();

	// PRE: La cola no est� vac�a
	// POS: Retorna el elemento de mayor prioridad en la cola sin eliminarlo
	const T& Cabeza() const;

	// PRE: -
	// POS: Retorna el largo de la cola
	Natural Largo() const;

	// PRE: El elemento e est� en la cola
	// POS: Retorna la instancia del elemento de mayor prioridad dentro de la cola.
	const T& ObtenerPrioridad(const T& e) const;

	// PRE: -
	// POS: Retorna true si y solo si el elemento e pertenece a la cola.
	bool Pertenece(const T& e) const;

	// PRE: -
	// POS: Retorna true si y solo si la cola esta vacia
	bool EstaVacia() const;

	// PRE: -
	// POS: Retorna true si y solo si la cola esta llena
	bool EstaLlena() const;

	// PRE: -
	// POS: Retorna un clon de la cola que no comparte memoria con ella
	Puntero<ColaPrioridad<T, P>> Clon() const;

	Iterador<T> ObtenerIterador() const;

protected:
	Natural cantElementos;
	Natural capacidadActual;
	Array<Tupla<T, P>> cola;
	Comparador<T> compElementos;
	Comparador<P> compPrioridades;

	//auxiliares
	void Crecer();
	void hundir(Natural pos);
	void flotar(Natural pos);
	void swap(Natural posElem1, Natural posElem2);
	T elemento;
};

#include "ColaPrioridadImp.cpp"
#endif

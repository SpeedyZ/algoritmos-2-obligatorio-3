#ifndef HEAP_CPP
#define HEAP_CPP

#include "Heap.h"

template<class T>
Heap<T>::~Heap()
{
	comparator = nullptr;
}

template<class T>
Heap<T>::Heap(nat length, Puntero<Comparador<T>> compIn)
{
	nextPos = 0;
	data = Array<T>(length);
	comparator = compIn;
}

template<class T>
void Heap<T>::Add(const T & item)
{
	if (IsFull())
		Expand();
	data[++nextPos] = item;
	Float(nextPos);
}

template<class T>
T & Heap<T>::GetRoot() const
{
	assert(!IsEmpty());
	return data[ROOT];
}

template<class T>
void Heap<T>::DeleteRoot()
{
	assert(!IsEmpty());
	data[ROOT] = data[nextPos--];
	Sink(ROOT);
}

template<class T>
bool Heap<T>::IsFull() const
{
	return nextPos == data.ObtenerLargo() - 1;
}

template<class T>
bool Heap<T>::IsEmpty() const
{
	return nextPos == 0;
}

template<class T>
bool Heap<T>::IsInternal(nat position) const
{
	return LeftSonPosition(position) <= nextPos;
}

template<class T>
bool Heap<T>::HasRightChild(nat position) const
{
	return RightSonPosition(position) <= nextPos;
}

template<class T>
void Heap<T>::Expand()
{
	Array<T> futureData = Array<T>(data.ObtenerLargo() * 2);
	for (nat i = 0; i < futureData.ObtenerLargo(); i++)
		futureData[i] = data[i];
}

template<class T>
nat Heap<T>::length() const
{
	return data.ObtenerLargo();
}

template<class T>
void Heap<T>::Float(nat position)
{
	if (position != ROOT)
	{
		int posFather = PosFather(position);
		if (comparator->EsMayor(data[posFather], data[position]))
		{
			Swap(posFather, position);
			Float(posFather);
		}
	}
}

template<class T>
void Heap<T>::Sink(nat position)
{
	if (IsInternal(position))
	{
		int newPos = PosGreatestSon(position);
		if (comparator->EsMayor(data[position], data[newPos]))
		{
			Swap(position, newPos);
			Sink(newPos);
		}
	}
}

template<class T>
void Heap<T>::Swap(nat item1, nat item2)
{
	T onHold = data[item1];
	data[item1] = data[item2];
	data[item2] = onHold;
}

template<class T>
nat Heap<T>::PosGreatestSon(nat position) const
{
	assert(IsInternal(position));
	int greatestPos = LeftSonPosition(position);
	if (HasRightChild(position))
	{
		int rightPos = RightSonPosition(position);
		if (comparator->EsMayor(data[rightPos], data[greatestPos]))
		{
			greatestPos = rightPos;
		}
	}
	return greatestPos;
}

template<class T>
nat Heap<T>::PosFather(nat position) const
{
	assert(position != ROOT);
	return position / 2;
}

template<class T>
nat Heap<T>::LeftSonPosition(nat position) const
{
	return position * 2;
}

template<class T>
nat Heap<T>::RightSonPosition(nat position) const
{
	return LeftSonPosition(position) + 1;
}

#endif // !HEAP_CPP

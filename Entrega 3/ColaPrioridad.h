#ifndef COLAPRIORIDAD_H
#define COLAPRIORIDAD_H

#include "Heap.h"
#include "Iterable.h"
#include "Tupla.h"

template<class P, class T>
class ColaPrioridad
{
public:
	~ColaPrioridad() {};

	ColaPrioridad();

	ColaPrioridad(const ColaPrioridad<P, T>& cola);

	bool esVacia();

	nat largo();

	T& Top() const;

	void Push(const P& prioridad, const T& dato);

	void Pop();

	void BorrarElemento(const T& dato);

	void CambiarPrioridad(const P& prioridad, const T& dato);

	void CambiarPrioridadTodos(const P& prioridad);
	
private:
	Heap<Tupla<P, T>> heap;

};
#include "ColaPrioridad.cpp"
#endif 
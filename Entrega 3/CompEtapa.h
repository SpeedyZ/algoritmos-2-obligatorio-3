#ifndef COMPETAPA_H
#define COMPETAPA_H

#include "Comparacion.h"
#include "Puntero.h"
#include "Etapa.h"

class CompEtapa : public Comparacion<Puntero<Etapa>>
{
	CompRetorno Comparar(const Puntero<Etapa>& et1, const Puntero<Etapa>& et2)
	{
		if (et1 == et2) return IGUALES;
		return DISTINTOS;
	}
};


#endif
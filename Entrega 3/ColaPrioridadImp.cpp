#ifndef COLAPRIORIDADIMP_CPP
#define COLAPRIORIDADIMP_CPP

#include "ColaPrioridadImp.h"
#include "ArrayIteracion.h"

template <class T, class P>
ColaPrioridadImp<T, P>::~ColaPrioridadImp() {}

template <class T, class P>
ColaPrioridadImp<T, P>::ColaPrioridadImp(Comparador<T> compT, Comparador<P> compP) {
	compElementos = compT;
	compPrioridades = compP;
	cola = Array<Tupla<T, P>>(TAM_COLA);
	cantElementos = 0;
	capacidadActual = TAM_COLA;
}

template <class T, class P>
void ColaPrioridadImp<T, P>::Encolar(const T& e, const P& p) {
	if (this->EstaLlena()) {
		Crecer();
	}
	cantElementos++;
	cola[cantElementos].AsignarDato1(e);
	cola[cantElementos].AsignarDato2(p);
	if (cantElementos != 1) {
		flotar(cantElementos);
	}
}

template <class T, class P>
const T& ColaPrioridadImp<T, P>::Desencolar() {
	elemento = cola[1].ObtenerDato1();
	cola[1].AsignarDato1(cola[cantElementos].ObtenerDato1());
	cola[1].AsignarDato2(cola[cantElementos].ObtenerDato2());
	cantElementos--;
	hundir(1);
	return elemento;
}

template <class T, class P>
const T& ColaPrioridadImp<T, P>::Cabeza() const {
	return (cola[1].ObtenerDato1());
}

template <class T, class P>
Natural ColaPrioridadImp<T, P>::Largo() const {
	return cantElementos;
}

template <class T, class P>
const T& ColaPrioridadImp<T, P>::ObtenerPrioridad(const T& e) const {
	for (Natural i = 1; i < cola.ObtenerLargo(); i++) {
		if (compElementos.SonIguales(cola[i].ObtenerDato1(), e)) {
			return cola[i].ObtenerDato1();
			break;
		}
	}
	return e;
}

template <class T, class P>
bool ColaPrioridadImp<T, P>::Pertenece(const T& e) const {
	for (Natural i = 1; i < cola.ObtenerLargo(); i++) {
		if (compElementos.SonIguales(cola[i].ObtenerDato1(), e)) {
			return true;
		}
	}
	return false;
}

template <class T, class P>
bool ColaPrioridadImp<T, P>::EstaVacia() const {
	return (cantElementos == 0);
}

template <class T, class P>
bool ColaPrioridadImp<T, P>::EstaLlena() const {
	return ((capacidadActual - 1) == (cantElementos));
}

template <class T, class P>
Puntero<ColaPrioridad<T, P>> ColaPrioridadImp<T, P>::Clon() const {
	return new ColaPrioridadImp<T, P>(*this);
}

template <class T, class P>
Iterador<T> ColaPrioridadImp<T, P>::ObtenerIterador() const {
	return Iterador<T>();
}

//auxiliares
template <class T, class P>
void ColaPrioridadImp<T, P>::Crecer() {
	capacidadActual += TAM_COLA;
	Array<Tupla<T, P>> nuevo = Array<Tupla<T, P>>(capacidadActual);
	for (Natural i = 1; i < cantElementos; i++) {
		nuevo[i].AsignarDato1(cola[i].ObtenerDato1());
		nuevo[i].AsignarDato2(cola[i].ObtenerDato2());
	}
	cola = nuevo;
}

template <class T, class P>
void ColaPrioridadImp<T, P>::hundir(Natural pos) {
	if ((pos * 2) <= cantElementos) {
		Natural hijoIzq = pos * 2;
		Natural hijoDer = ((pos * 2) + 1);
		if ((hijoDer <= cantElementos) && (compPrioridades.EsMayor(cola[hijoDer].ObtenerDato2(), cola[hijoIzq].ObtenerDato2()))) {
			hijoIzq++;
		}
		if (compPrioridades.EsMayor(cola[hijoIzq].ObtenerDato2(), cola[pos].ObtenerDato2())) {
			swap(hijoIzq, pos);
			hundir(hijoIzq);
		}
	}
}

template <class T, class P>
void ColaPrioridadImp<T, P>::flotar(Natural pos) {
	if (pos > 1) {
		Natural padre = pos / 2;
		if (compPrioridades.EsMayor(cola[pos].ObtenerDato2(), cola[padre].ObtenerDato2())) {
			swap(pos, padre);
			flotar(padre);
		}
	}
}

template <class T, class P>
void ColaPrioridadImp<T, P>::swap(Natural posElem1, Natural posElem2) {
	Tupla<T, P> aux;
	aux.AsignarDato1(cola[posElem1].ObtenerDato1());
	aux.AsignarDato2(cola[posElem1].ObtenerDato2());
	cola[posElem1].AsignarDato1(cola[posElem2].ObtenerDato1());
	cola[posElem1].AsignarDato2(cola[posElem2].ObtenerDato2());
	cola[posElem2].AsignarDato1(aux.ObtenerDato1());
	cola[posElem2].AsignarDato2(aux.ObtenerDato2());
}
#endif
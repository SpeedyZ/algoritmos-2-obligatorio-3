#ifndef FHASHETAPA_H
#define FHASHETAPA_H

#include "Etapa.h"
#include "FuncionHash.h"

class FuncionEtapa : public FuncionHash<Puntero<Etapa>>
{
	nat CodigoDeHash(const Puntero<Etapa>& et) const {
		nat suma = 0;
		Cadena imp = et->Fichas().Imprimir();
		nat pA = et->Fichas().PrioridadA();
		nat pB = et->Fichas().PrioridadB();
		nat movs = et->Movimientos();
		for (nat i = 0; i < imp.ObtenerLargo(); i++) suma += (imp[i] + pA)*(imp[i] + pB)*(i + 1 + movs);
		return suma;
	}
};

#endif
#ifndef ETAPA_H
#define ETAPA_H

#include "Tablero.h"

typedef unsigned int nat;

class Etapa 
{
public:
	Etapa() {};
	Etapa(const Tablero& t);
	Etapa(const Tablero& t, Puntero<Etapa> et);
	Etapa(const Etapa& et);
	
	bool operator==(const Etapa& et) const;
	Etapa& operator=(const Etapa& et);
	Puntero<Etapa> Anterior();
	const Tablero& Fichas() const;
	nat Movimientos() const;
	nat Prioridad() const;

private:
	Puntero<Etapa> ant;
	nat mov;
	Tablero tab;
};

//#include "Etapa.cpp"
#endif
#ifndef ITERADORTABLERO_H
#define ITERADORTABLERO_H

#include "Puntero.h"
#include "NodoLista.h"
#include "Iteracion.h"
#include "Etapa.h"

typedef unsigned int nat;

template <class Tablero>
class IteradorTablero : public Iteracion<Tablero> {
public:

	~IteradorTablero() {}
	IteradorTablero(const Tablero& tab);
	IteradorTablero(const Puntero<Etapa>& et);

	void Avanzar();
	void Reiniciar();
	bool HayElemento() const;
	const Tablero& ElementoActual() const;
	Puntero <Iteracion<Tablero>> Clonar() const;

private:
	void InsertarLista(const Tablero& tab);
	Tablero tablero;
	Puntero<NodoLista<Tablero>> ppio;
	Puntero<NodoLista<Tablero>> actual;
};

#include "IteradorTablero.cpp"

#endif


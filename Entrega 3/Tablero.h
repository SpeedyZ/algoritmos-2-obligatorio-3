#ifndef TABLERO_H
#define TABLERO_H

#include "Matriz.h"
#include "Cadena.h"
#include "Iterador.h"
#include <cmath>

typedef unsigned int nat;

class Tablero{
public : 
	Tablero() {};

	//Construye un tablero dada una matriz de NxN bloques. 
	Tablero(Matriz<int> bloques);

	//Constructor copia
	Tablero(const Tablero& t);

	//Retorna el numero de bloques fuera de lugar 
	nat PrioridadA() const;

	//Retorna la suma de distancias de los bloques a su posicion final. 
	nat PrioridadB() const;

	//Decide si esta posicion es igual a otra.
	bool operator==(const Tablero& t) const;

	//Operador de asignacion
	Tablero& operator=(const Tablero& t);

	//Retorna un iterador de las posiciones vecinas a esta. 
	Iterador<Tablero> Vecinos() const;

	//Retorna el tablero. El cero representa la posición libre.
	Matriz<int> ObtenerTablero() const;

	//Retorna la representación del tablero como cadena.
	Cadena Imprimir() const;

private:
	Matriz<int> mat;

	//AUX
	void copia(const Matriz<int>& matOri, Matriz<int>& matCop);
};

//#include "Tablero.cpp"
#endif


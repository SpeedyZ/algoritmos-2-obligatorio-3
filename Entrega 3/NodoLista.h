#ifndef NODOLISTA_H
#define NODOLISTA_H

template <class T>
class NodoLista {
public:
	T dato;
	Puntero<NodoLista<T>> sig;

	NodoLista(const T &e = T(), Puntero<NodoLista<T>> s = nullptr) : dato(e), sig(s) {}
	NodoLista(const Puntero<NodoLista<T>> &n) : dato(n->dato), sig(n->sig) {}

	NodoLista<T> &operator=(const Puntero<NodoLista<T>> &n) { dato = n->dato; sig = n->sig; }

	virtual ~NodoLista() {}
};

#endif
#ifndef ETAPA_CPP
#define ETAPA_CPP

#include "Etapa.h"

Etapa::Etapa(const Tablero & t)
{
	tab = t;
	mov = 0;
	ant = NULL;
}

Etapa::Etapa(const Tablero & t, Puntero<Etapa> et)
{
	tab = t;
	mov = et->mov + 1;
	ant = et;
}

Etapa::Etapa(const Etapa & et)
{
	*this = et;
}

bool Etapa::operator==(const Etapa & et) const
{
	return tab == et.tab;
}

Etapa & Etapa::operator=(const Etapa & et)
{
	if (this != &et) {
		tab = et.tab;
		mov = et.mov;
		ant = et.ant;
	}
	return *this;
}

Puntero<Etapa> Etapa::Anterior()
{
	return ant;
}

const Tablero& Etapa::Fichas() const
{
	return tab;
}

nat Etapa::Movimientos() const
{
	return mov;
}

nat Etapa::Prioridad() const
{
	/*nat p = Fichas().PrioridadB();
	return p + mov;*/
	return Fichas().PrioridadB() + mov;
}

#endif

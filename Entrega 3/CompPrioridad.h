#ifndef COMPPRIORIDAD_H
#define COMPPRIORIDAD_H

#include "Comparacion.h"
#include "Puntero.h"

typedef unsigned int nat;

class CompPrioridad : public Comparacion<nat>
{
	CompRetorno Comparar(const nat& p1, const nat& p2) const 
	{	
		if (p1 > p2) return MENOR;
		if (p1 < p2) return MAYOR;
		if (p1 == p2) return IGUALES;
		return DISTINTOS;
	}
};

#endif
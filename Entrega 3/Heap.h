#ifndef HEAP_H
#define HEAP_H

//#include "Iterable.h"
#include "Array.h"

template <class T>
class Heap// : public Iterable<T>
{
public:
	~Heap();

	Heap(nat length = 1000, Puntero<Comparador<T>> compIn = new Comparador<T>(Comparador<T>::Default));

	virtual void Add(const T& item);

	T& GetRoot() const;

	virtual void DeleteRoot();

protected:
	void Float(nat position);

	void Sink(nat position);

	void Swap(nat initialPos, nat finalPos);

	nat PosGreatestSon(nat position) const;

	nat PosFather(nat position) const;

	nat LeftSonPosition(nat position) const;

	nat RightSonPosition(nat position) const;

	bool IsFull() const;

	bool IsEmpty() const;

	bool IsInternal(nat position) const;

	bool HasRightChild(nat position) const;

	void Expand();

	nat length() const;

private:
	const nat ROOT = 1;
	nat nextPos;
	Array<T> data;
	Puntero<Comparador<T>> comparator;
};

#include "Heap.cpp"

#endif // !HEAP_H
